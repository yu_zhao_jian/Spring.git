package test.controller;

import it.yuzuojian.com.annotation.mvc.Controller;
import it.yuzuojian.com.annotation.mvc.RequestMapping;
import it.yuzuojian.com.annotation.mvc.RequestParam;
import it.yuzuojian.com.annotation.mvc.ResponseBody;
import it.yuzuojian.com.pojo.mvc.ModelAndView;
import it.yuzuojian.com.pojo.mvc.ModelMap;
import test.javaBean.Student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author 于作箭
 * @date 2020/7/14 - 15:27
 */
@Controller
public class StudentController {
   /* @RequestMapping("/test1")
    public String test2(Model model, Student student){
        model.addAttribute("name",student.getName());
        model.addAttribute("password",student.getPassword());
        model.addAttribute("age",student.getAge());
        System.out.println(student);
        return "showInfo";
    }
    @RequestMapping(value = "/restfule",method = RequestMethod.GET)
    public String testRestfulerGet(){
        System.out.println("get");
        return "showInfo";
    }
//    @RequestMapping(value = "/restfule",method = RequestMethod.GET)
//    public String testRestfulerGet1(@PathVariable("name") String name){
//        System.out.println("get"+name);
//        return "showInfo";
//    }
    @RequestMapping(value = "restfule/{a}",method = RequestMethod.GET)
    public String testRestfulePost(@PathVariable("a") String a,Model model, Student student)throws CustomException  {
        model.addAttribute("name",student.getName());
        model.addAttribute("password",student.getPassword());
        model.addAttribute("age",student.getAge());
        System.out.println(student+"      "+a);
        try {
            int i=1/0;
        } catch (Exception e) {
            e.printStackTrace();
            throw  new CustomException("自定义错误...");
        }
        return "showInfo";
    }

    @RequestMapping(value = "testSetSessionAttributs")
        public String testSetSessionAttributs(Model model){
        model.addAttribute("sessionMsg","yuzuojian");
        return "showInfo";
    }
    @RequestMapping(value = "testGetSessionAttributs")
    public String testGetSessionAttributs(ModelMap map){
        System.out.println((String)map.get("sessionMsg"));
        return "showInfo";
    }
    @RequestMapping(value = "testDelSessionAttributs")
    public String testDelSessionAttributs(ModelAndView map, SessionStatus status){
        map.getModel().remove("sessionMsg");
//        status.setComplete();
        return "showInfo";
    }

    @RequestMapping(value ="/testJson")
    public @ResponseBody Student testJson(@RequestBody Student student){
        System.out.println(student);
        Student response=new Student();
        response.setName("ha");
        response.setPassword("123456");
        response.setAge(new Date());
        return response;
    }*/
    @RequestMapping("/testFileUpload")
    public String testFileUpload() throws IOException {
        return "showInfo";
    }
   @RequestMapping("/login")
   public ModelAndView login(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) throws IOException {
       String name1 = request.getParameter("name");
       String password1 = request.getParameter("password");
       /*System.out.println(student);
       System.out.println(name1+"-->"+password1);
       if(checkbox==null){
           System.out.println("checkbox-->null");
       }else {
           for (String s : checkbox) {
               System.out.println(s);
           }
       }
       System.out.println(time);*/
       //model.removeAttribute("checkbox");
       //model.addAttribute("test","1");
       request.setAttribute("test","2");
       modelAndView.setView("forward:showInfo");
       return modelAndView;
   }
    @ResponseBody
    @RequestMapping("/testJson")
    public Student testJson(String name,@RequestParam("name") String password, Date time, String[] checkbox,
                            Student student, @RequestParam("password") String name1) throws IOException {
        System.out.println(name+password+time+checkbox+name1);
        return student;
    }


}
