package test.config;

import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.handle.mvc.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Component
public class MVCConfig implements WebMvcConfigurer {
    @Override
    public List<String> addStaticResourceURL() {
        List<String> list = new ArrayList<>();
        list.add("/js/[\\s\\S]*.js");
        list.add("/css/[\\s\\S]*.css");
        list.add("/image/[\\s\\S]*.png");
        list.add("/image/[\\s\\S]*.jpg");
        return list;
    }
}
