package test.handle;

import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.handle.mvc.DateHandle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*@Component*/
public class MyDateHandle implements DateHandle {
    @Override
    public Date format(String date) throws ParseException {
        SimpleDateFormat oldFormatter = new SimpleDateFormat("YYYY-MM-DD");
        SimpleDateFormat newFormatter = new SimpleDateFormat("YYYY/MM/DD hh:mm:ss");
        Date parse = oldFormatter.parse(date);
        String format = newFormatter.format(parse);
        return newFormatter.parse(format);
    }
}
