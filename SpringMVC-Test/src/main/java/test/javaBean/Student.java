package test.javaBean;

import it.yuzuojian.com.annotation.ioc.Component;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * @author 于作箭
 * @date 2020/7/14 - 15:34
 */
@Component
public class Student implements Serializable {
    private String name;
    private String password;
    private String[] checkbox;
    private Date time;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String[] getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(String[] checkbox) {
        this.checkbox = checkbox;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", checkbox=" + Arrays.toString(checkbox) +
                '}';
    }
}
