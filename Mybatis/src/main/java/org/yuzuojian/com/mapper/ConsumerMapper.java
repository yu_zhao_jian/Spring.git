package org.yuzuojian.com.mapper;

import org.yuzuojian.com.pojo.Consumer;

import java.util.List;

public interface ConsumerMapper {

    public List<Consumer> findAll();

    public Consumer findByIds(List ids);

    public Consumer findByConsumer(Consumer consumer);

    public void insert(Consumer consumer);

    public void delete(Consumer consumer);

    public void update(Consumer consumer);

}
