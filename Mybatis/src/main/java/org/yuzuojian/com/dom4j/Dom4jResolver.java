package org.yuzuojian.com.dom4j;

import com.alibaba.druid.pool.DruidDataSource;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.yuzuojian.com.pojo.*;

import java.io.InputStream;
import java.util.*;

public class Dom4jResolver {

    public MybatisConfig resolverConfig(InputStream inputStream) throws DocumentException {
        MybatisConfig mybatisConfig = new MybatisConfig();
        DruidDataSource druidDataSource = new DruidDataSource();
        SAXReader reader = new SAXReader();
        Document document = reader.read(inputStream);
        Element configuration = document.getRootElement();
        Iterator iterator = configuration.elementIterator();
        while (iterator.hasNext()){
            Element next = (Element)iterator.next();
            if(next.getName().equals("environments")){
                Element environment = next.element("environment");
                Element dataSource = environment.element("dataSource");
                List<Element> propertys = dataSource.elements("property");
                for (Element property : propertys) {
                    Attribute name = property.attribute("name");
                    Attribute value = property.attribute("value");
                    if(name.getValue().equals("driver")){
                        druidDataSource.setDriverClassName(value.getValue());
                    }else if(name.getValue().equals("url")){
                        druidDataSource.setUrl(value.getValue());
                    }else if(name.getValue().equals("username")){
                        druidDataSource.setUsername(value.getValue());
                    }else {
                        druidDataSource.setPassword(value.getValue());
                    }
                }
            }else {

                    Element aPackage = next.element("package");
                    Element mapperLocations = next.element("mapper-locations");
                    MapperScan mapperScan1 = new MapperScan();
                    mybatisConfig.setMapperScan(new MapperScan(aPackage.getStringValue(),mapperLocations.getStringValue()));


            }
        }
        mybatisConfig.setDataSource(druidDataSource);
        return mybatisConfig;
    }

    public Mapper resolverMapper(InputStream inputStream) throws DocumentException, ClassNotFoundException {
        Mapper mapper = new Mapper();
        Map<String, Function> functions = new HashMap<>();
        Map<String, ResultMap> resultMaps = new HashMap<>();
        SAXReader reader = new SAXReader();
        Document document = reader.read(inputStream);
        Element mapperElement = document.getRootElement();
        Attribute namespace = mapperElement.attribute("namespace");
        mapper.setNamespace(Class.forName(namespace.getValue()));
        Iterator iterator = mapperElement.elementIterator();
        while (iterator.hasNext()){
            Element next = (Element)iterator.next();
            if(next.getName().equals("resultMap")){
                ResultMap resultMap = new ResultMap();
               List<Result> results = new ArrayList<>();
                Attribute id = next.attribute("id");
                Attribute type = next.attribute("type");
                List<Element> result = next.elements("result");
                for (Element element : result) {
                    Attribute property = element.attribute("property");
                    Attribute column = element.attribute("column");
                    Attribute jdbcType = element.attribute("jdbcType");
                    Result result1 = new Result(property.getValue(),column.getValue(),jdbcType.getValue());
                    results.add(result1);
                }
                Element idElement = next.element("id");
                Attribute property = idElement.attribute("property");
                Attribute column = idElement.attribute("column");
                Attribute jdbcType = idElement.attribute("jdbcType");
                Id idResult = new Id(property.getValue(),column.getValue(),jdbcType.getValue());
                resultMap.setIdResult(idResult);
                resultMap.setType(type.getValue());
                resultMap.setResults(results);
                resultMap.setId(id.getValue());
                resultMaps.put(id.getValue(),resultMap);
            }else {
                String sql = next.getData().toString();
                String name = next.getName();
                Attribute id = next.attribute("id");
                Attribute parameterType = next.attribute("parameterType");
                Attribute resultMap = next.attribute("resultMap");
                Function function = new Function(name,name,sql,resultMap.getValue(),parameterType.getValue());
                functions.put(id.getValue(),function);
            }
        }
        mapper.setResultMaps(resultMaps);
        mapper.setFunctions(functions);
        return mapper;
    }
}
