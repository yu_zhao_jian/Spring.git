package org.yuzuojian.com.pojo;

import org.dom4j.DocumentException;
import org.yuzuojian.com.dom4j.Dom4jResolver;

import java.io.InputStream;

public class SqlSessionFactoryBuilder {
    private Dom4jResolver dom4jResolver = new Dom4jResolver();
    public SqlSessionFactory build(InputStream inputStream) throws DocumentException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactory();
        sqlSessionFactory.setMybatisConfig(dom4jResolver.resolverConfig(inputStream));
        return sqlSessionFactory;
    }
}
