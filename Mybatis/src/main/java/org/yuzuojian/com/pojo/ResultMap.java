package org.yuzuojian.com.pojo;

import java.util.List;
import java.util.Map;

public class ResultMap {
    private String id;
    private String type;
    private Id idResult;
    private List<Result> results;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Id getIdResult() {
        return idResult;
    }

    public void setIdResult(Id idResult) {
        this.idResult = idResult;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
