package org.yuzuojian.com.pojo;

import org.yuzuojian.com.cglib.CglibDynamicAopProxy;

import java.util.Map;

public class SqlSession {
    private SqlSessionFactory sqlSessionFactory;
    private Map<Class,Mapper> mappers;


    public Map<Class, Mapper> getMappers() {
        return mappers;
    }

    public void setMappers(Map<Class, Mapper> mappers) {
        this.mappers = mappers;
    }

    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public Object getMapper(Class clazz) throws InstantiationException, IllegalAccessException {
        CglibDynamicAopProxy cglibDynamicAopProxy = new CglibDynamicAopProxy(clazz,mappers,sqlSessionFactory);

        return cglibDynamicAopProxy.getProxy();
    }
}
