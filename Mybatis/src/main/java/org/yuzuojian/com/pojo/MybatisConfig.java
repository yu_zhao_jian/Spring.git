package org.yuzuojian.com.pojo;

import javax.sql.DataSource;
import java.util.Map;

public class MybatisConfig {
    private DataSource dataSource;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public MapperScan getMapperScan() {
        return mapperScan;
    }

    public void setMapperScan(MapperScan mapperScan) {
        this.mapperScan = mapperScan;
    }

    private MapperScan mapperScan;


}
