package org.yuzuojian.com.pojo;

public class MapperScan {
    private String apackage;
    private String local;

    public MapperScan(String apackage, String local) {
        this.apackage = apackage;
        this.local = local;
    }
    public MapperScan() {
    }

    public String getApackage() {
        return apackage;
    }

    public void setApackage(String apackage) {
        this.apackage = apackage;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
