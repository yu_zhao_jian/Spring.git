package org.yuzuojian.com.pojo;

public class Function {
    private String sqlType;
    private String funcName;
    private String sql;
    private String resultMap;
    private String parameterType;

    public Function(String sqlType, String funcName, String sql, String resultMap, String parameterType) {
        this.sqlType = sqlType;
        this.funcName = funcName;
        this.sql = sql;
        this.resultMap = resultMap;
        this.parameterType = parameterType;
    }

    public Function() {

    }

    public String getSqlType() {
        return sqlType;
    }

    public void setSqlType(String sqlType) {
        this.sqlType = sqlType;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getResultMap() {
        return resultMap;
    }

    public void setResultMap(String resultType) {
        this.resultMap = resultType;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }
}
