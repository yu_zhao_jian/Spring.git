package org.yuzuojian.com.pojo;


import java.util.Map;

public class Mapper {
    private Map<String,Function> functions;
    private Map<String,ResultMap> resultMaps;
    private Class namespace;

    public Class getNamespace() {
        return namespace;
    }

    public void setNamespace(Class namespace) {
        this.namespace = namespace;
    }

    public Map<String, Function> getFunctions() {
        return functions;
    }

    public void setFunctions(Map<String, Function> functions) {
        this.functions = functions;
    }

    public Map<String, ResultMap> getResultMaps() {
        return resultMaps;
    }

    public void setResultMaps(Map<String, ResultMap> resultMaps) {
        this.resultMaps = resultMaps;
    }
}
