package org.yuzuojian.com;

import org.dom4j.DocumentException;
import org.yuzuojian.com.mapper.ConsumerMapper;
import org.yuzuojian.com.pojo.Consumer;
import org.yuzuojian.com.pojo.SqlSession;
import org.yuzuojian.com.pojo.SqlSessionFactory;
import org.yuzuojian.com.pojo.SqlSessionFactoryBuilder;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IOException, DocumentException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        InputStream resourceAsStream =Application.class.getClassLoader().getResourceAsStream("MybatisConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        ConsumerMapper consumer = (ConsumerMapper)sqlSession.getMapper(ConsumerMapper.class);
        List<Consumer> all = consumer.findAll();
        System.out.println(all);


    }
}
