# Spring 



## IOC

- 想将bean解析成BeanDefinition对象（存放bean的信息）
- getBean()的时候从一级缓存和二级缓存中取，加上是否是单例，判断是否返回还是继续重新获取
- 首先判断是否需要动态代练生成代理对象（aop）
- 将对象放入二级缓存中（防止互相依赖）
- 对象属性注入，如果属性对象没有就getBean（）递归
- 将对象放入一级缓存中



## AOP

- 初始化，将BeanDefinitionMap中的切面解析成GPAopConfig对象放入list中
- getBean的时候将对象放入GPAdviceSupport中，并且判断是否需要aop
- 判断是否需要跟家GPAopConfig的@pointcut 3个属性判断
- 如果需要，先将GPAopConfig中的所有方法放到map中，然后根据需要被代理的class对象方法是否代理去进行代理





## MVC

- 主要有一个DispatcherServlet对象，所有请求在这里经过
- DispatcherServlet初始化：会去解析所有Controller的方法，将其放入map中
- 如果有请求过来就去匹配map获取对应的methon执行就好

##### 静态资源

- 需要用户自己设置自定义正则表达式去匹配需要放行的静态资源
- 如果匹配将静态资源以流的形式输出即可

## mybatis

- 自定义xml配置文件标签属性 *.dtd文件
- 解析配置文件成对象
- 通过动态代理（我使用cglib）生成代理对象
- 通过jdbc去执行sql

#### 目前sql执行的参数扩展和返回结果没有实现，主要是select标签里面有其他的标签比如foreach标签，不会解析拼装sql