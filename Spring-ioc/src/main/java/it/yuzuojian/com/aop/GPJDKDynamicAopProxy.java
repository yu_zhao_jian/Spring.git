package it.yuzuojian.com.aop;

import jdk.nashorn.internal.runtime.regexp.JoniRegExp;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class GPJDKDynamicAopProxy implements InvocationHandler {
    private GPAdviceSupport config;
    public GPJDKDynamicAopProxy(GPAdviceSupport config) {
        this.config=config;
    }

    public Object getProxy() {
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), this.config.getTargetClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return AopFactory.getProxy(config,method,args);
    }
}
