package it.yuzuojian.com.aop;


import java.lang.reflect.Method;

public class GPAdvice {
    private Object aspect;
    private Method adviceMethod;
    private String throwName;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    private int order;

    public Object getAspect() {
        return aspect;
    }

    public GPAdvice(Object aspect, Method adviceMethod,int order) {
        this.aspect = aspect;
        this.adviceMethod = adviceMethod;
        this.order=order;
    }

    public void setAspect(Object aspect) {
        this.aspect = aspect;
    }

    public Method getAdviceMethod() {
        return adviceMethod;
    }

    public void setAdviceMethod(Method adviceMethod) {
        this.adviceMethod = adviceMethod;
    }

    public String getThrowName() {
        return throwName;
    }

    public void setThrowName(String throwName) {
        this.throwName = throwName;
    }
}
