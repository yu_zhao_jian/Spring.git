package it.yuzuojian.com.aop;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class CglibDynamicAopProxy  implements MethodInterceptor {
    private GPAdviceSupport config;
    public CglibDynamicAopProxy(GPAdviceSupport config) {
        this.config=config;
    }

    public Object getProxy() {
        Enhancer enhancer = new Enhancer();
        //设置父类
        enhancer.setSuperclass(this.config.getTargetClass());
        //设置回调
        enhancer.setCallback(this);
        //设置类加载器
        enhancer.setClassLoader(this.config.getTargetClass().getClassLoader());
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        return AopFactory.getProxy(config,method,objects);
    }
}
