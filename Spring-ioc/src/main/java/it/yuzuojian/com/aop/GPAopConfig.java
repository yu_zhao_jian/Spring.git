package it.yuzuojian.com.aop;

public class GPAopConfig {
    private String pointCut_package;
    private String[] pointCut_class;
    private String[] pointCut_method;
    private String aspectClass;
    private String aspectBefore;
    private String aspectAfter;
    private String afterReturning;
    private String afterThrowing;
    private String around;
    private int order;


    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


    public String getAspectClass() {
        return aspectClass;
    }

    public void setAspectClass(String aspectClass) {
        this.aspectClass = aspectClass;
    }

    public String getAspectBefore() {
        return aspectBefore;
    }

    public void setAspectBefore(String aspectBefore) {
        this.aspectBefore = aspectBefore;
    }

    public String getAspectAfter() {
        return aspectAfter;
    }

    public void setAspectAfter(String aspectAfter) {
        this.aspectAfter = aspectAfter;
    }

    public String getAfterReturning() {
        return afterReturning;
    }

    public void setAfterReturning(String afterReturning) {
        this.afterReturning = afterReturning;
    }

    public String getAfterThrowing() {
        return afterThrowing;
    }

    public void setAfterThrowing(String afterThrowing) {
        this.afterThrowing = afterThrowing;
    }

    public String getAround() {
        return around;
    }

    public void setAround(String around) {
        this.around = around;
    }

    public String getPointCut_package() {
        return pointCut_package;
    }

    public void setPointCut_package(String pointCut_package) {
        this.pointCut_package = pointCut_package;
    }

    public String[] getPointCut_class() {
        return pointCut_class;
    }

    public void setPointCut_class(String[] pointCut_class) {
        this.pointCut_class = pointCut_class;
    }

    public String[] getPointCut_method() {
        return pointCut_method;
    }

    public void setPointCut_method(String[] pointCut_method) {
        this.pointCut_method = pointCut_method;
    }
}
