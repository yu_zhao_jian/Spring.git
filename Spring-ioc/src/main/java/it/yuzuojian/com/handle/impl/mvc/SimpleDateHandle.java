package it.yuzuojian.com.handle.impl.mvc;

import it.yuzuojian.com.handle.mvc.DateHandle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

public class SimpleDateHandle implements DateHandle {
    @Override
    public Date format(String date) throws ParseException {
        if(date==null || date.length()==0 || date.equals("")){
            return null;
        }
        SimpleDateFormat oldFormatter = new SimpleDateFormat("YYYY-MM-DD");
        SimpleDateFormat newFormatter = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");
        Date parse = oldFormatter.parse(date);
        String format = newFormatter.format(parse);
        return newFormatter.parse(format);
    }
}
