package it.yuzuojian.com.handle.mvc;

import java.text.ParseException;
import java.util.Date;

public interface DateHandle {
    public Date format(String date) throws ParseException;
}
