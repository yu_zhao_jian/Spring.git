package it.yuzuojian.com.handle.mvc;

import java.util.List;

public interface WebMvcConfigurer {
    List<String> addStaticResourceURL();
}
