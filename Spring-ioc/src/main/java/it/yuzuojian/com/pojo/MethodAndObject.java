package it.yuzuojian.com.pojo;

import java.lang.reflect.Method;

public class MethodAndObject {
    private Method method;
    private Object object;

    public boolean isReturnJson() {
        return returnJson;
    }

    public void setReturnJson(boolean returnJson) {
        this.returnJson = returnJson;
    }

    private boolean returnJson;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
