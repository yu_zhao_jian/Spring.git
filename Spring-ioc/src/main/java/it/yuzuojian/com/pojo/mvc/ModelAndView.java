package it.yuzuojian.com.pojo.mvc;



public class ModelAndView {
    private Object view;

    /** Model Map */
    private ModelMap model;

    public Object getView() {
        return view;
    }

    public void setView(Object view) {
        this.view = view;
    }

    public ModelMap getModel() {
        return model;
    }

    public void setModel(ModelMap model) {
        this.model = model;
    }
}
