package it.yuzuojian.com.pojo;

public class BeanDefinition {
    private Class clazz;
    private String beanName;
    private String scope;
    private boolean beanPostProcessor;
    private boolean aspect;
    private boolean controller;
    private boolean dateHandle;
    private boolean webMvcConfigurer;

    public boolean isWebMvcConfigurer() {
        return webMvcConfigurer;
    }

    public void setWebMvcConfigurer(boolean webMvcConfigurer) {
        this.webMvcConfigurer = webMvcConfigurer;
    }

    public boolean isDateHandle() {
        return dateHandle;
    }

    public void setDateHandle(boolean dateHandle) {
        this.dateHandle = dateHandle;
    }

    public boolean isController() {
        return controller;
    }

    public void setController(boolean controller) {
        this.controller = controller;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }



    public boolean isAspect() {
        return aspect;
    }

    public void setAspect(boolean aspect) {
        this.aspect = aspect;
    }



    public boolean isBeanPostProcessor() {
        return beanPostProcessor;
    }

    public void setBeanPostProcessor(boolean beanPostProcessorOrder) {
        this.beanPostProcessor = beanPostProcessorOrder;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "BeanDefinition{" +
                "clazz=" + clazz +
                ", beanName='" + beanName + '\'' +
                ", scope='" + scope + '\'' +
                ", beanPostProcessor=" + beanPostProcessor +
                ", aspect=" + aspect +
                '}';
    }
}
