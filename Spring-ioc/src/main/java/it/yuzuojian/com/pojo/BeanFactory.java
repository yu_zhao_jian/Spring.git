package it.yuzuojian.com.pojo;

import it.yuzuojian.com.handle.mvc.DateHandle;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class BeanFactory {
    private static ConcurrentHashMap<String, BeanDefinition> beanDefinitions = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Object> originalObjects = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Object> singletonObjects  = new ConcurrentHashMap<>();
    private static List<BeanPostProcessorOrder> beanPostProcessorOrders = new ArrayList<>();
    private static List<DateHandle> dateHandles = new ArrayList<>();
    private static List<String> mvcStaticResources = new ArrayList<>();

    public static List<String> getMvcStaticResources() {
        return mvcStaticResources;
    }

    public static void setMvcStaticResources(List<String> mvcStaticResources) {
        BeanFactory.mvcStaticResources = mvcStaticResources;
    }

    public static List<BeanPostProcessorOrder> getBeanPostProcessorOrders() {
        return beanPostProcessorOrders;
    }

    public static void setBeanPostProcessorOrders(List<BeanPostProcessorOrder> beanPostProcessorOrders) {
        BeanFactory.beanPostProcessorOrders = beanPostProcessorOrders;
    }

    public static ConcurrentHashMap<String, BeanDefinition> getBeanDefinitions() {
        return beanDefinitions;
    }

    public static void setBeanDefinitions(ConcurrentHashMap<String, BeanDefinition> beanDefinitions) {
        BeanFactory.beanDefinitions = beanDefinitions;
    }

    public static ConcurrentHashMap<String, Object> getOriginalObjects() {
        return originalObjects;
    }

    public static void setOriginalObjects(ConcurrentHashMap<String, Object> originalObjects) {
        BeanFactory.originalObjects = originalObjects;
    }

    public static ConcurrentHashMap<String, Object> getSingletonObjects() {
        return singletonObjects;
    }

    public static void setSingletonObjects(ConcurrentHashMap<String, Object> singletonObjects) {
        BeanFactory.singletonObjects = singletonObjects;
    }

    public static void setBeanDefinition(BeanDefinition beanDefinition){
       beanDefinitions.put(beanDefinition.getBeanName(),beanDefinition);
   }
    public static List<DateHandle> getDateHandles() {
        return dateHandles;
    }

    public static void setDateHandles(List<DateHandle> dateHandles) {
        BeanFactory.dateHandles = dateHandles;
    }
   public static BeanDefinition getBeanDefinition(String beanName){
       return beanDefinitions.get(beanName);
   }
    public static void setOriginalObject(String beanName,Object object){
        originalObjects.put(beanName,object);
    }
    public static Object getOriginalObject(String beanName){
        return originalObjects.get(beanName);
    }
    public static void setSingletonObjects(String beanName,Object object){
        singletonObjects.put(beanName,object);
    }
    public static Object getSingletonObject(String beanName){
        return singletonObjects.get(beanName);
    }
/*    public void setSingletonObject(String beanName){
        return singletonObjects.put(beanName);
    }*/
}
