package it.yuzuojian.com.pojo.mvc;

import java.util.LinkedHashMap;


public class ModelMap extends LinkedHashMap<String, Object> {

    /**
     * Construct a new, empty {@code ModelMap}.
     */
    public ModelMap() {
    }

    /**
     * Construct a new {@code ModelMap} containing the supplied attribute
     * under the supplied name.
     * @see #addAttribute(String, Object)
     */
    public ModelMap(String attributeName, Object attributeValue) {
        addAttribute(attributeName, attributeValue);
    }



    public ModelMap addAttribute(String attributeName,  Object attributeValue) {
        put(attributeName, attributeValue);
        return this;
    }

    public ModelMap removeAttribute(String attributeName) {
        remove(attributeName);
        return this;
    }




}
