package it.yuzuojian.com.pojo;

import it.yuzuojian.com.handleiInterface.BeanPostProcessor;

public class BeanPostProcessorOrder {
    private int order;
    private BeanPostProcessor beanPostProcessor;

    public BeanPostProcessorOrder(int order, BeanPostProcessor beanPostProcessor) {
        this.order = order;
        this.beanPostProcessor = beanPostProcessor;
    }
    public BeanPostProcessorOrder() {

    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public BeanPostProcessor getBeanPostProcessor() {
        return beanPostProcessor;
    }

    public void setBeanPostProcessor(BeanPostProcessor beanPostProcessor) {
        this.beanPostProcessor = beanPostProcessor;
    }

    @Override
    public String toString() {
        return "BeanPostProcessorOrder{" +
                "order=" + order +
                ", beanPostProcessor=" + beanPostProcessor +
                '}';
    }
}
