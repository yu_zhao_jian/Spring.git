package it.yuzuojian.com.annotation.ioc;

import java.lang.annotation.*;

@Target(ElementType.LOCAL_VARIABLE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Resource {
    String value() ;
}
