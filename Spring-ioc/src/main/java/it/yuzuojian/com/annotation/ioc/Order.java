package it.yuzuojian.com.annotation.ioc;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * value 小的先执行
 */
public @interface Order {
    int value() default 0;

}
