package it.yuzuojian.com.annotation.mvc;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * value 小的先执行
 */
public @interface RequestMapping {
    String value();

}
