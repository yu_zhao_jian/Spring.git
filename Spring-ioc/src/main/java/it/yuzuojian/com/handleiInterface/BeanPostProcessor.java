package it.yuzuojian.com.handleiInterface;

/**
 * bean的后置处理器
 */
public interface BeanPostProcessor {
    /**
     * 初始化之前执行
     * @param bean
     * @param beanName
     * @return
     */
    default Object postProcessBeforeInitialization(Object bean,String beanName){
        return bean;
    }

    /**
     * 初始化之后执行
     * @param bean
     * @param beanName
     * @return
     */
    default Object postProcessAfterInitialization(Object bean,String beanName){
        return bean;
    }
}
