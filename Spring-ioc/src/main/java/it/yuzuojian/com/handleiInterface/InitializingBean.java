package it.yuzuojian.com.handleiInterface;

/**
 * 初始化Bean接口，在属性赋值之后执行
 */
public interface InitializingBean {
    public void afterPropertiesSet();
}
