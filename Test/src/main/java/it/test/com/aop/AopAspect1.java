package it.test.com.aop;

import it.yuzuojian.com.annotation.aop.After;
import it.yuzuojian.com.annotation.aop.Aspect;
import it.yuzuojian.com.annotation.aop.Before;
import it.yuzuojian.com.annotation.aop.PointCut;
import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Order;


@Aspect
//@Component
@Order(2)
public class AopAspect1 {
    @PointCut(package_name = "it.test.com.pojo" )
    public void pointCut(){

    }
    @Before
    public void before(){
        System.out.println("before2");
    }
    @After
    public void after(){
        System.out.println("after2");
    }
}
