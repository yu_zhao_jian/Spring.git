package it.test.com.aop;

import it.yuzuojian.com.annotation.aop.*;
import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Order;


@Aspect
//@Component
@Order()
public class AopAspect {
    @PointCut(package_name = "it.test.com.pojo",class_name = "AopTest",method_name = "say")
    public String pointCut(){
        return "public .* it.test.com.pojo.AopTest.*(..)";
    }
    @Before
    public void before(){
        System.out.println("before1");
    }
    @After
    public void after(){
        System.out.println("after1");
    }
    @AfterThrowing
    public void afterThrowing(){
        System.out.println("afterThrowing1");
    }

}
