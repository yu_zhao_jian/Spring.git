package it.test.com.pojo;

import it.yuzuojian.com.annotation.aop.Aspect;
import it.yuzuojian.com.annotation.aop.Before;
import it.yuzuojian.com.annotation.aop.PointCut;
import it.yuzuojian.com.annotation.ioc.Autowired;
import it.yuzuojian.com.annotation.ioc.Component;
import lombok.Data;

import java.io.Serializable;

@Data
@Component
public class AopTest {

    public int a=1;

    public void say(){

        System.out.println(a);
    }
}
