package it.test.com.pojo;

import it.yuzuojian.com.annotation.ioc.Autowired;
import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Scope;
import it.yuzuojian.com.handleiInterface.InitializingBean;

@Scope("111")
@Component("Student")
public class Student implements InitializingBean {
    private  Integer id=1;
    private String name="yuzuojian";
    @Autowired
    private  Teacher teacher;


    @Override
    public void afterPropertiesSet() {
        System.out.println("student InitializingBean");
    }
}
