package it.test.com.config;

import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Order;
import it.yuzuojian.com.handleiInterface.BeanPostProcessor;
/*@Component
@Order(1)*/
public class MyBeanPostProcessor1 implements BeanPostProcessor{
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println(beanName+"---> before1");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println(beanName+"---> after1");
        return bean;
    }
}
