package it.test.com.config;

import it.test.com.pojo.Student;
import it.yuzuojian.com.annotation.ioc.Autowired;
import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Configuration;
import lombok.Data;
import lombok.ToString;

@Configuration
@Component("configTest")
@ToString
@Data
public class ConfigTest {
    @Autowired
    public Student student;
    /*@Bean("Teacher")
    public Teacher teacher(){
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setName("yuzuojian");
        return teacher;
    }*/

}
