package it.test.com.config;

import it.yuzuojian.com.annotation.ioc.Component;
import it.yuzuojian.com.annotation.ioc.Order;
import it.yuzuojian.com.handleiInterface.BeanPostProcessor;

/*@Component
@Order(2)*/
public class MyBeanPostProcessor2 implements BeanPostProcessor{
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println(beanName+"---> before2");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println(beanName+"---> after2");
        return bean;
    }
}
