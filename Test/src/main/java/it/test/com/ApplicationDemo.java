package it.test.com;



import it.test.com.config.ConfigTest;
import it.test.com.pojo.AopTest;
import it.test.com.pojo.Student;
import it.test.com.pojo.Teacher;
import it.yuzuojian.com.ApplicationContext;
import it.yuzuojian.com.annotation.ioc.ComponentScan;

@ComponentScan("it.test.com")
public class ApplicationDemo {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        ApplicationContext applicationContext = new ApplicationContext(ApplicationDemo.class);
        Object teacher = applicationContext.getBean("Teacher");
        Student student = (Student)applicationContext.getBean("Student");

        ConfigTest configTest = (ConfigTest)applicationContext.getBean("ConfigTest") ;
/*        System.out.println((Student)applicationContext.getBean("Student"));
        System.out.println((Student)applicationContext.getBean("Student"));
        System.out.println((Student)applicationContext.getBean("Student"));

        System.out.println((Teacher)applicationContext.getBean("Teacher"));
        System.out.println((Teacher)applicationContext.getBean("Teacher"));
        System.out.println((Teacher)applicationContext.getBean("Teacher"));*/
        AopTest aopTest = (AopTest) applicationContext.getBean("AopTest");
        aopTest.say();

    }
}
